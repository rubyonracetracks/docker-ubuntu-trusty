#!/bin/bash

ABBREV='dev'
OWNER='rubyonracetracks'
DISTRO='ubuntu'
SUITE='trusty'
DOCKER_IMAGE="$OWNER/$DISTRO-$SUITE-$ABBREV"
CONTAINER="container-$DISTRO-$SUITE-$ABBREV"

bash setup.sh $ABBREV $DOCKER_IMAGE $CONTAINER
