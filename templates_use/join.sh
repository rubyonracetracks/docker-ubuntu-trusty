#!/bin/bash

# Check for regular user login
if [ ! $( id -u ) -ne 0 ]; then
  echo 'You must be a regular user to run this script.'
  exit 2
fi

CONTAINER='<CONTAINER>'

echo '******'
echo 'NOTICE'
echo 'If this join.sh script does not work, then there is probably no'
echo 'running container to join.  You likely need to run the'
echo 'download_new_image.sh, reset.sh, or resume.sh script first.'
echo

DATE=`date +%Y_%m%d_%H%M`

is='/bin/bash' # Initial script to run
hs=$PWD/shared # Host machine shared directory
ds='/home/winner/shared' # Docker shared directory

echo '-----------------------------------'
echo "Joining Docker container $CONTAINER"
wait
docker exec -it $CONTAINER /bin/bash
